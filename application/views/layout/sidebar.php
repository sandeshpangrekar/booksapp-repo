
<section class="sidebar">
	<a href="javascript:void(0)" class="logo"><img src="<?php echo base_url('assets/images/logo5.jpg'); ?>"></a>
	<a href="javascript:void(0)" class="mini-logo"><img src="<?php echo base_url('assets/images/small_logo.jpg'); ?>"></a>
	<ul>
		<li><a href="<?= base_url(); ?>dashboard" <?php if ($this->uri->segment(1) == "dashboard"){ echo "class='active'"; } ?> ><i class="fal fa-home"></i><span>Dashboard</span></a></li>

		<li><a href="<?= base_url(); ?>books" <?php if ($this->uri->segment(1) == "books"){ echo "class='active'"; } ?> ><i class="fal fa-users"></i><span>Books</span></a></li>

	</ul>

</section>