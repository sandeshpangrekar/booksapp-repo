<!DOCTYPE html>
<html>
<head>
	<title><?= isset($title) ? $title : 'Title'; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta Http-Equiv="Cache-Control" Content="no-cache">
	<meta Http-Equiv="Pragma" Content="no-cache">
	<meta Http-Equiv="Expires" Content="0">
	<meta Http-Equiv="Pragma-directive: no-cache">
	<meta Http-Equiv="Cache-directive: no-cache">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url(); ?>assets/css/all.min.css" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/jquery.dataTables.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/select2.min.css'); ?>" />
	<link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css'); ?>">
	<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript">var base_url = "<?= base_url(); ?>";</script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/toastr/toastr.min.css">
    <script src="<?= base_url(); ?>assets/toastr/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/datepicker.css">
    <script type="text/javascript" src="<?= base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
	
</head>
<body>
<div id="loader" style="display: none;"></div>
<?php include "sidebar.php"; ?>

<div class="custom-container">
	<nav>
		<div class="row mx-0">
			<div class="col-md-1">
				<a href="javascript:void(0)" class="toggle-menu"><i class="fal fa-bars"></i></a>
			</div>
			<div class="col-md-11">
			
			</div>
		</div>
	</nav>
