<?php include APPPATH.'views/layout/header.php' ?>

<style type="text/css">
  .tdbold{
    font-weight: bold !important;
  }
</style>

<nav class="breadcrumb-outer">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= base_url(); ?>dashboard">Dashboard</a></li>
        <li class="breadcrumb-item active">Books</li>
    </ol>
</nav>
<div class="container-fluid">
    <div class="row title">
    	<div class="col-md-6">
    		<h4>Books</h4>
    	</div>
    	<div class="col-md-6 text-right">
    		<a href="<?= base_url(); ?>books/add" class="btn btn-sm btn-primary">
          <i class="fal fa-plus mr-1"></i>Add New Book
        </a>

        <button type="button" id="btn_export_excel" class="btn btn-sm btn-primary">
          <i class="fal fa-file-excel mr-1"></i>Export As Excel
        </button>

    	</div>
    </div>


    <table id="table_id" class="display">
        <thead>
            <tr>
                <th>Sr No</th>
                <th>Book Title</th>
                <th>Book Code</th>
                <th>ISBN</th>
                <th class="text-center">Book ID</th>
                <th>Author</th>
                <th>Publication</th>
                <th>Year</th>
                <th>Price ($)</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>

            <?php if(!empty($booksData)){ ?>
                <?php for($i=0;$i<count($booksData);$i++){ ?>

            <tr>
              <td><?= $i+1; ?></td>
              <td><?= $booksData[$i]['book_title']; ?></td>
              <td><?= $booksData[$i]['book_code']; ?></td>
              <td><?= $booksData[$i]['isbn']; ?></td>
              <td  class="text-center"><?= $booksData[$i]['book_id']; ?></td>
              <td><?= $booksData[$i]['author']; ?></td>
              <td><?= $booksData[$i]['publication']; ?></td>
              <td><?= $booksData[$i]['year']; ?></td>
              <td><?= '$'.$booksData[$i]['price']; ?></td>
              
              
              <td class="text-center">

                    <a href="<?= base_url(); ?>books/edit/<?= $booksData[$i]['book_id']; ?>" class="btn btn-sm btn-primary" data-toggle-tooltip="tooltip" title="Edit" data-placement="top" > 
                      <i class="fal fa-pencil"></i>
                    </a>

                     <a href="<?= base_url(); ?>books/view/<?= $booksData[$i]['book_id']; ?>" class="btn btn-sm btn-success" data-toggle-tooltip="tooltip" title="View" data-placement="top"> 
                      <i class="fal fa-eye"></i>
                     </a>



                    <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deleteModal<?= $booksData[$i]['book_id']; ?>" data-toggle-tooltip="tooltip" title="Delete" data-placement="top"> 
                      <i class="fal fa-trash"></i>
                    </a> 


                    <!-- start of view modal -->
                    <div class="modal" id="viewModal<?= $booksData[$i]['book_id']; ?>">
                      <div class="modal-dialog" style="overflow-y: initial !important;">
                        <div class="modal-content">

                      
                          <!-- Modal Header -->
                          <div class="modal-header">
                            <h4 class="modal-title">View Book</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>

                          <!-- Modal body -->
                          <div class="modal-body" style="text-align: left !important;">
                            
                            <table class="table table-bordered">
                              <tbody>

                                <tr>
                                  <td class="tdbold" >Book Title</td>
                                  <td><?= $booksData[$i]['book_title']; ?></td>
                                </tr>

                                <tr>
                                  <td class="tdbold">Book Code</td>
                                  <td><?= $booksData[$i]['book_code']; ?></td>
                                </tr>

                                <tr>
                                  <td class="tdbold">ISBN</td>
                                  <td><?= $booksData[$i]['isbn']; ?></td>
                                </tr>

                                <tr>
                                  <td class="tdbold">Book ID</td>
                                  <td><?= $booksData[$i]['book_id']; ?></td>
                                </tr>

                                <tr>
                                  <td class="tdbold">Author</td>
                                  <td><?= $booksData[$i]['author']; ?></td>
                                </tr>


                                <tr>
                                  <td class="tdbold">Publication</td>
                                  <td><?= $booksData[$i]['publication']; ?></td>
                                </tr>


                                <tr>
                                  <td class="tdbold">Year</td>
                                  <td><?= $booksData[$i]['year']; ?></td>
                                </tr>

                                <tr>
                                  <td class="tdbold">Price</td>
                                  <td><?= '$'.$booksData[$i]['price']; ?></td>
                                </tr>
                             

                                <tr>
                                  <td class="tdbold">Created On</td>
                                  <td><?php echo date('dS M Y h:i A', strtotime($booksData[$i]['created_on'])); ?></td>
                                </tr>

                                <tr>
                                  <td class="tdbold">Updated On</td>
                                  <td><?php echo date('dS M Y h:i A', strtotime($booksData[$i]['updated_on'])); ?></td>
                                </tr>

                              </tbody>
                            </table>
                            
                          </div>

                          <!-- Modal footer -->
                          <div class="modal-footer">
                            <a href="<?= base_url(); ?>books/edit/<?= $booksData[$i]['book_id']; ?>" class="btn btn-primary mr-1">Edit</a>
                            <button type="button" class="btn btn-danger close_popup" data-dismiss="modal">Close</button>
                          </div>
                        
                        </div>
                      </div>
                    </div>
                <!-- end of view modal -->





                <!-- start of delete modal -->
                    <div class="modal" id="deleteModal<?= $booksData[$i]['book_id']; ?>">
                      <div class="modal-dialog" style="overflow-y: initial !important;">
                        <div class="modal-content">

                          
                          <!-- Modal Header -->
                          <div class="modal-header">
                            <h4 class="modal-title">Delete Book</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>

                          <!-- Modal body -->
                          <div class="modal-body" style="text-align: left !important;">
                            
                            Are you sure to 'Delete' this book?
                            
                          </div>

                          <!-- Modal footer -->
                          <div class="modal-footer">
                            <button type="button" onclick="deleteBook(<?php echo $booksData[$i]['book_id']; ?>);" class="btn btn-primary mr-1">Yes</button>
                            <button type="button" class="btn btn-danger close_popup" data-dismiss="modal">No</button>
                          </div>

                        </div>
                      </div>
                    </div>
                <!-- end of delete modal -->

              </td>



            </tr>

                <?php } ?>
            <?php } ?>

           
        </tbody>
    </table>
</div>

<?php include APPPATH.'views/layout/footer.php' ?>


<script>
  toastr.options = {
      "preventDuplicates": true,
  }
</script>

<script type="text/javascript">
$(document).ready( function () {

    $('#table_id').dataTable({
    });

} );
</script>

<script>
$(document).ready(function(){
  $("body").tooltip({ selector: '[data-toggle-tooltip=tooltip]' });
});
</script>


<script type="text/javascript">
$(document).ready(function () {
 $('#btn_export_excel').click(function () {

    $("#loader").show();
    $.ajax({   
    url: "<?php echo base_url(); ?>"+"books/export", 
    type: "POST",
    data: {
            action: 'export'
          }, 
    dataType: "json", 
    timeout: 0,
    beforeSend: function() {
    },
    success: function(response) 
    {
       $("#loader").hide();
       if(response.status=="success" && response.exported_url!="")
       {
          window.location.href= response.exported_url;
       } 

       else if(response.status=="failed" && response.exported_url=="")
       {
          console.log(response);
          alert('No Data Available For Export!');
       } 

       else
       {
          console.log(response);
          alert('Something went wrong!');
       }                    
    },
     error: function(jqXHR, textStatus, errorThrown)
     {
        $("#loader").hide();
        //console.log(textStatus, errorThrown);
        alert('Something went wrong!');
     }

   });

  });
 });
</script>


<script type="text/javascript">
  
 function deleteBook(book_id)
 {
    //$("#loader").show();
    $.ajax({
        url:base_url+'books/delete',
        type:'POST',
        data:{ book_id:book_id },
        dataType:'JSON',
        timeout: 0,
        beforeSend: function() {
        },
        success: function(response){
          
          $("#loader").hide();
          if(response.status=="success")
          {
             location.href=response.redirect_url;
          }

          else
          {
             toastr.error(response.msg);
          }

        },
        error: function(jqXHR, textStatus, errorThrown)
        {
          $("#loader").hide();
         //console.log(textStatus, errorThrown);
         alert('Something went wrong!');
        }

       });
 }

</script>


