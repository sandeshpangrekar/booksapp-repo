<?php include APPPATH.'views/layout/header.php' ?>

<nav class="breadcrumb-outer">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?= base_url(); ?>dashboard">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?= base_url(); ?>books">Books</a></li>
        <li class="breadcrumb-item active">Edit Book</li>
    </ol>
</nav>

<div class="container-fluid">
    <div class="row title">
      <div class="col-md-12">
        <h4>Edit Book</h4>
      </div>
    </div>
        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label class="form-label">Book Title<sup class="text-danger"> *</sup></label>
                    <input type="text" name="book_title" id="book_title" class="form-control" required="required" value="<?= $bookData->book_title; ?>" />
                    <span id="book_title_msg"></span>
                </div>
                <div class="form-group">
                    <label class="form-label">Book Code<sup class="text-danger"> *</sup></label>
                    <input type="text" name="book_code" id="book_code" class="form-control" required="required" value="<?= $bookData->book_code; ?>" />
                    <span id="book_code_msg"></span>
                </div>
                <div class="form-group">
                    <label class="form-label">ISBN<sup class="text-danger"> *</sup></label>
                    <input type="text" name="isbn" id="isbn" class="form-control" required="required" value="<?= $bookData->isbn; ?>" />
                    <span id="isbn_msg"></span>
                </div>

                <div class="form-group">
                    <label class="form-label">Author<sup class="text-danger"> *</sup></label>
                    <input type="text" name="author" id="author" class="form-control" required="required" value="<?= $bookData->author; ?>" />
                    <span id="author_msg"></span>
                </div>
                
                
                <div class="form-group">
                    <label class="form-label">Publication<sup class="text-danger"> *</sup></label>
                    <input type="text" name="publication" id="publication" class="form-control" required="required" value="<?= $bookData->publication; ?>" />
                    <span id="publication_msg"></span>
                </div>


                <div class="form-group">
                    <label class="form-label">Year<sup class="text-danger"> *</sup></label>
                    <input type="text" name="year" id="year" class="form-control" required="required" value="<?= $bookData->year; ?>" />
                    <span id="year_msg"></span>
                </div>

                <div class="form-group">
                    <label class="form-label">Price<sup class="text-danger"> *</sup></label>
                    <input type="text" name="price" id="price" class="form-control" required="required" value="<?= $bookData->price; ?>" />
                    <span id="price_msg"></span>
                </div>


                <div class="row mt-4">
                    <div class="col-md-12">
                        <button type="button" id="submitBtn" class="btn btn-sm btn-primary">Submit</button>
                        <a href="<?= base_url(); ?>books" class="btn btn-sm btn-danger">Cancel</a>
                    </div>
                </div>

            </div>
        </div>
</div>

<?php include APPPATH.'views/layout/footer.php' ?>

<script>
  toastr.options = {
      "preventDuplicates": true,
  }
</script>



<script type="text/javascript">
    
   $("#submitBtn").click(function(){

       var book_title=$("#book_title").val();
       var book_code=$("#book_code").val();
       var isbn=$("#isbn").val();
       var author=$("#author").val();
       var publication=$("#publication").val();
       var year=$("#year").val();
       var price=$("#price").val();
       var book_id="<?php echo $book_id; ?>";
       var error=0;

       book_title = book_title ? book_title : null;
       book_code = book_code ? book_code : null;
       isbn = isbn ? isbn : null;
       author = author ? author : null;
       publication = publication ? publication : null;
       year = year ? year : null;
       price = price ? price : null;
    
       if(book_title=="" || book_title==null)
       {
          $("#book_title_msg").empty();
          $("#book_title_msg").html('<p style="color:red">This field is required!</p>');
          error++;
       }

       else
       {
          $("#name_msg").empty();
       }

       if(book_code=="" || book_code==null)
       {
          $("#book_code_msg").empty();
          $("#book_code_msg").html('<p style="color:red">This field is required!</p>');
          error++;
       }

       else
       {
          $("#book_code_msg").empty();
       }

       if(isbn=="" || isbn==null)
       {
          $("#isbn_msg").empty();
          $("#isbn_msg").html('<p style="color:red">This field is required!</p>');
          error++;
       }

       else
       {
          $("#isbn_msg").empty();
       }

       if(author=="" || author==null)
       {
          $("#author_msg").empty();
          $("#author_msg").html('<p style="color:red">This field is required!</p>');
          error++;
       }

       else
       {
          $("#author_msg").empty();
       }

       if(publication=="" || publication==null)
       {
          $("#publication_msg").empty();
          $("#publication_msg").html('<p style="color:red">This field is required!</p>');
          error++;
       }

       else
       {
          $("#publication_msg").empty();
       }

       if(year=="" || year==null)
       {
          $("#year_msg").empty();
          $("#year_msg").html('<p style="color:red">This field is required!</p>');
          error++;
       }

       else
       {
          $("#year_msg").empty();
       }

       if(price=="" || price==null)
       {
          $("#price_msg").empty();
          $("#price_msg").html('<p style="color:red">This field is required!</p>');
          error++;
       }

       else
       {
          $("#price_msg").empty();
       }

       if(error > 0)
       {
          return false;
       }

       //$("#loader").show();

       $.ajax({
        url:base_url+'books/edit/'+book_id,
        type:'POST',
        data:{ 
         
          book_title : book_title,
          book_code : book_code,
          isbn : isbn,
          author : author,
          publication : publication,
          year : year,
          price : price

        },
        dataType:'JSON',
        timeout: 0,
        beforeSend: function() {
        },
        success: function(response){
          
          $("#loader").hide();
          if(response.status=="success")
          {
             location.href=response.redirect_url;
          }

          else
          {
             toastr.error(response.msg);
          }

        },
        error: function(jqXHR, textStatus, errorThrown)
        {
         $("#loader").hide();
         //console.log(textStatus, errorThrown);
         alert('Something went wrong!');
        }

       })

   });

</script>