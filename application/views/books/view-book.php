<?php include APPPATH.'views/layout/header.php' ?>
<style type="text/css">
  .tdbold{
    font-weight: bold;
  }
</style>
<nav class="breadcrumb-outer">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>dashboard">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>books">Books</a></li>
    <li class="breadcrumb-item active">View Book</li>
  </ol>
</nav>
<div class="container-fluid">
  <div class="row title">
    <div class="col-md-12">
      <h4>View Book</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <table class="table table-striped">
            <tbody>
              
              <tr>
                <td class="tdbold">Book ID</td>
                <td><?= $bookData->book_id; ?></td>
              </tr>

              <tr>
                <td class="tdbold">Book Title</td>
                <td><?= $bookData->book_title; ?></td>
              </tr>

              <tr>
                <td class="tdbold" >Book Code</td>
                <td><?= $bookData->book_code; ?></td>
              </tr>

              <tr>
                <td class="tdbold" >ISBN</td>
                <td><?= $bookData->isbn; ?></td>
              </tr>

              <tr>
                <td class="tdbold" >Author</td>
                <td><?= $bookData->author; ?></td>
              </tr>

              <tr>
                <td class="tdbold" >Publication</td>
                <td><?= $bookData->publication; ?></td>
              </tr>

              <tr>
                <td class="tdbold" >Year</td>
                <td><?= $bookData->year; ?></td>
              </tr>

              <tr>
                <td class="tdbold" >Price</td>
                <td><?= '$'.$bookData->price; ?></td>
              </tr>

              <tr>
                <td class="tdbold">Created On</td>
                <td><?php echo date('dS M Y h:i A', strtotime($bookData->created_on)); ?></td>
              </tr>
              <tr>
                <td class="tdbold">Updated On</td>
                <td><?php echo date('dS M Y h:i A', strtotime($bookData->updated_on)); ?></td>
              </tr>

            </tbody>
          </table>

          <div align="center" class="mt-4 mb-4">
            <a href="<?= base_url(); ?>books/edit/<?= $book_id ?>" class="btn btn-primary">Edit</a>
            <a href="<?= base_url(); ?>books" class="btn btn-secondary">Back To Books</a>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<?php include APPPATH.'views/layout/footer.php' ?>
<script>
  toastr.options = {
      "preventDuplicates": true,
  }
</script>