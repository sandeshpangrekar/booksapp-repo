<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Books extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('role')!="Admin")
	    {
	       $this->toastr->error('Invalid Session!');
	       redirect(base_url()."login");
	    }
	    $this->load->model('books_model', 'books_model');
	}

	public function index()
	{
	   $data['title'] = 'Books';
       $data['booksData'] = $this->books_model->getAll();
	   $this->load->view('books/books-list', $data);
	}

	public function add()
	{
	   if($_SERVER['REQUEST_METHOD']=="POST")
	   {
	   	  $this->form_validation->set_rules('book_title', 'Book Title', 'required');
	   	  $this->form_validation->set_rules('book_code', 'Email', 'required');
          $this->form_validation->set_rules('isbn', 'ISBN', 'required');
          $this->form_validation->set_rules('author', 'Author', 'required');
          $this->form_validation->set_rules('publication', 'Publication', 'required');
          $this->form_validation->set_rules('year', 'Year', 'required');
          $this->form_validation->set_rules('price', 'Price', 'required');
           
          if($this->form_validation->run()===false)
          {
             echo json_encode(array("status"=>"failed", "msg"=>"Please enter required input fields!", "redirect_url"=>base_url()."login"));
             exit(); 
          }

	   	  $data=[];
	   	  $data['book_title']=$this->input->post('book_title');
	   	  $data['book_code']=$this->input->post('book_code');
	   	  $data['isbn'] = $this->input->post('isbn');
	   	  $data['author']=$this->input->post('author');
	   	  $data['publication']=$this->input->post('publication');
          $data['year']=$this->input->post('year');
          $data['price']=$this->input->post('price');
	   	  $data['created_on']=date('Y-m-d H:i:s');
	   	  $data['updated_on']=date('Y-m-d H:i:s');

          $this->books_model->insert($data);

          
          $this->toastr->success('Added successfully!');
		  echo json_encode(array("status"=>"success", "msg"=>"Added successfully!", "redirect_url"=>base_url()."books"));
          exit();
	   }

	   else
	   {
	   	  $data['title'] = 'Add Books';
	      $this->load->view('books/add-book', $data);
	   }
	}

	public function edit($book_id)
	{
	   if($_SERVER['REQUEST_METHOD']=="POST")
	   {
	   	  $this->form_validation->set_rules('book_title', 'Book Title', 'required');
	   	  $this->form_validation->set_rules('book_code', 'Email', 'required');
          $this->form_validation->set_rules('isbn', 'ISBN', 'required');
          $this->form_validation->set_rules('author', 'Author', 'required');
          $this->form_validation->set_rules('publication', 'Publication', 'required');
          $this->form_validation->set_rules('year', 'Year', 'required');
          $this->form_validation->set_rules('price', 'Price', 'required');
           
          if($this->form_validation->run()===false)
          {
             echo json_encode(array("status"=>"failed", "msg"=>"Please enter required input fields!", "redirect_url"=>base_url()."login"));
             exit(); 
          }

          $data=[];
	   	  $data['book_title']=$this->input->post('book_title');
	   	  $data['book_code']=$this->input->post('book_code');
	   	  $data['isbn'] = $this->input->post('isbn');
	   	  $data['author']=$this->input->post('author');
	   	  $data['publication']=$this->input->post('publication');
          $data['year']=$this->input->post('year');
          $data['price']=$this->input->post('price');
	   	  $data['updated_on']=date('Y-m-d H:i:s');
	   	  
          $this->books_model->update($data, $book_id);
          $this->toastr->success('Updated successfully!');
		  echo json_encode(array("status"=>"success", "msg"=>"Updated successfully!", "redirect_url"=>base_url()."books"));
          exit();
	   }

	   else
	   {
	   	  $data['title'] = 'Edit Book';
	   	  $data['book_id'] = $book_id;
	   	  $data['bookData']=$this->books_model->get($book_id);
	      $this->load->view('books/edit-book', $data);
	   }
	}


	public function view($book_id)
	{
   	   $data['title'] = 'View Book';
   	   $data['book_id'] = $book_id;
   	   $data['bookData']=$this->books_model->get($book_id);
       $this->load->view('books/view-book', $data);
	}

	public function updateStatus()
	{
		$book_id=$this->input->post('book_id');
		$action=$this->input->post('action');
		if($action=="Activate")
	    {
	       $status_flash='Activated';
	    }
	    else
	    {
	       $status_flash='Deactivated';
	    }
		$this->books_model->updateStatus($book_id, $action);
		$this->toastr->success($status_flash.' successfully!');
		echo json_encode(array("status"=>"success", "msg"=>$status_flash." successfully!", "redirect_url"=>base_url()."books"));
        exit();
	}

	public function delete()
	{
		$book_id=$this->input->post('book_id');
		$this->books_model->delete($book_id);
		$this->toastr->success('Deleted successfully!');
		echo json_encode(array("status"=>"success", "msg"=>"Deleted successfully!", "redirect_url"=>base_url()."books"));
        exit();
	}

	public function export()
	{
		if($_SERVER['REQUEST_METHOD']=="POST")
		{
                $data=$this->books_model->getAllBooksData();
	    
			    if(count($data) > 0)
			    {
			    	ini_set('memory_limit', -1);//-1
			    	require_once APPPATH . "third_party/PHPExcel-1.8/Classes/PHPExcel.php";
			        $objPHPExcel = new PHPExcel();
			        $objPHPExcel->setActiveSheetIndex(0);
			        // set Header
		            $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sr No');
			        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Book Title');
			        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Book Code');
			        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'ISBN');
			        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Author'); 
			        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Publication'); 
			        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Year'); 
			        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Price'); 
			        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Created On');
			        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Updated On');
			        
		        // set Row
		        $rowCount = 2;
		        $sr_no=0;
		        foreach ($data as $key => $value) {
		            $sr_no++;
		            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $sr_no);
		            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $value['book_title']);
		            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $value['book_code']);
		            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $value['isbn']);
		            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $value['author']);
		            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $value['publication']);
		            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $value['year']);
		            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, '$'.$value['price']);
		            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, date('dS M Y h:i A', strtotime($value['created_on'])));
		            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, date('dS M Y h:i A', strtotime($value['updated_on'])));
		            $rowCount++;
		        }
			        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			        $fileName = time().'.xlsx';
			        $objWriter->save(FCPATH.'exports/'.$fileName);
			        // download file
			        header("Content-Type: application/vnd.ms-excel");
			        $exported_url= base_url().'exports/'.$fileName;
			        echo json_encode(array("status"=>"success", "exported_url"=>$exported_url));
			        exit(); 
			    }
			    else
			    {
			        echo json_encode(array("status"=>"failed", "exported_url"=>''));
			        exit(); 
			    }
		    }
		    else
		    {
		        redirect(base_url()."books");
		    }
	}

}
