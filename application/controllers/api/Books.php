<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Books extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        header ("Access-Control-Allow-Origin: *");
        header ("Access-Control-Expose-Headers: Content-Length, X-JSON");
        header ("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        header ("Access-Control-Allow-Headers: *");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        $this->load->model('books_model');
    }

    public function getallbooks_get()
    {
        try{
        
            $booksData=$this->books_model->getAll();

            if($booksData)
            {
               $message = [
                'status' => 1,
                'message' => 'Books fetched successfully!',
                'data'=>$booksData
               ];
            }

            else
            {
               $message = [
                'status' => 0,
                'message' => 'No data found!',
                'data'=>null
               ];
            }


        $this->set_response($message, REST_Controller::HTTP_OK); // HTTP_OK (200) being the HTTP response code
      }catch(Exception $e){
        $message = [
                'message' => 'Something went wrong!'
            ];
        $this->set_response($message, REST_Controller::HTTP_OK);
      }
    }


    public function getbook_post()
    {
        try{

            $jsonData = json_decode(file_get_contents('php://input'), true);
            $book_id=$jsonData['book_id'];
            $bookData=$this->books_model->get($book_id);

            if($bookData)
            {
               $message = [
                'status' => 1,
                'message' => 'Book fetched successfully!',
                'data'=>$bookData
               ];
            }

            else
            {
               $message = [
                'status' => 0,
                'message' => 'No data found!',
                'data'=>null
               ];
            }


        $this->set_response($message, REST_Controller::HTTP_OK); // HTTP_OK (200) being the HTTP response code
      }catch(Exception $e){
        $message = [
                'status' => 0,
                'message' => 'Something went wrong!'
            ];
        $this->set_response($message, REST_Controller::HTTP_OK);
      }
    }

    public function add_post()
    {
        try{
        $jsonData = json_decode(file_get_contents('php://input'), true);

        $book_title=$jsonData['book_title'];
        $book_code=$jsonData['book_code'];
        $isbn=$jsonData['isbn'];
        $author=$jsonData['author'];
        $publication=$jsonData['publication'];
        $year=$jsonData['year'];
        $price=$jsonData['price'];
        $updated_on=$jsonData['updated_on'];
        
        if(empty($book_title) || empty($book_code) || empty($isbn) || empty($author) || empty($publication) || empty($year) || empty($price) ) 
        {
            $message = ['status' => 0,'message'=>"Invalid Inputs"];
            $this->response($message, REST_Controller::HTTP_OK);
        }

        else
        {
           $data=[];
           $data['book_title']=$book_title;
           $data['book_code']=$book_code;
           $data['isbn'] = $isbn;
           $data['author']=$author;
           $data['publication']=$publication;
           $data['year']=$year;
           $data['price']=$price;
           $data['created_on']=date('Y-m-d H:i:s');
           $data['updated_on']=date('Y-m-d H:i:s');
           $result = $this->books_model->insert($data);

           echo $this->db->last_query();exit();

            if($result!=false)
            {
               $message = [
                'status' => 1,
                'message'=>"Insert successful.",
                'data'=>$data
               ];

               $this->set_response($message, REST_Controller::HTTP_OK); // HTTP_OK (200)
           }

           
           else
           {
              $message = [
                'status' => 0,
                'message'=>"Something went wrong. Insert failed."
               ];

               $this->set_response($message, REST_Controller::HTTP_OK); // HTTP_OK (200)
           }

        }

     }catch(Exception $e){
        $message = [
                'status' => 0,
                'message' => 'Something went wrong!'
            ];
        $this->set_response($message, REST_Controller::HTTP_OK);
     }
        
    }


    
    public function edit_post()
    {
        try{
        $jsonData = json_decode(file_get_contents('php://input'), true);

        $book_id=$jsonData['book_id'];
        $book_title=$jsonData['book_title'];
        $book_code=$jsonData['book_code'];
        $isbn=$jsonData['isbn'];
        $author=$jsonData['author'];
        $publication=$jsonData['publication'];
        $year=$jsonData['year'];
        $price=$jsonData['price'];
        $updated_on=$jsonData['updated_on'];
        
        if(empty($book_id) || empty($book_title) || empty($book_code) || empty($isbn) || empty($author) || empty($publication) || empty($year) || empty($price) ) 
        {
            $message = ['status' => 0,'message'=>"Invalid Inputs"];
            $this->response($message, REST_Controller::HTTP_OK);
        }

        else
        {
           $data=[];
           $data['book_title']=$book_title;
           $data['book_code']=$book_code;
           $data['isbn'] = $isbn;
           $data['author']=$author;
           $data['publication']=$publication;
           $data['year']=$year;
           $data['price']=$price;
           $data['updated_on']=date('Y-m-d H:i:s');
           $result = $this->books_model->update($data, $book_id);

           $data['book_id']=$book_id;

           if($result!=false)
           {
               $message = [
                'status' => 1,
                'message'=>"Update successful.",
                'data'=>$data
               ];

               $this->set_response($message, REST_Controller::HTTP_OK); // HTTP_OK (200)
           }

           
           else
           {
              $message = [
                'status' => 0,
                'message'=>"Something went wrong. Insert failed."
               ];

               $this->set_response($message, REST_Controller::HTTP_OK); // HTTP_OK (200)
           }

        }

     }catch(Exception $e){
        $message = [
                'status' => 0,
                'message' => 'Something went wrong!'
            ];
        $this->set_response($message, REST_Controller::HTTP_OK);
     }
        
    }



    public function delete_post()
    {
        try{
        $jsonData = json_decode(file_get_contents('php://input'), true);
        $book_id=$jsonData['book_id'];
        
        if(empty($book_id)) 
        {
            $message = ['status' => 0,'message'=>"Invalid Inputs"];
            $this->response($message, REST_Controller::HTTP_OK);
        }

        else
        {
           $data=[];
           $data['book_id']=$book_id;
           $result = $this->books_model->delete($book_id);

           if($result!=false)
           {
               $message = [
                'status' => 1,
                'message'=>"Delete successful.",
                'data'=>$data
               ];

               $this->set_response($message, REST_Controller::HTTP_OK); // HTTP_OK (200)
           }
           
           else
           {
              $message = [
                'status' => 0,
                'message'=>"Something went wrong. Insert failed."
               ];

               $this->set_response($message, REST_Controller::HTTP_OK); // HTTP_OK (200)
           }

        }

     }catch(Exception $e){
        $message = [
                'status' => 0,
                'message' => 'Something went wrong!'
            ];
        $this->set_response($message, REST_Controller::HTTP_OK);
     }
        
    }


}
