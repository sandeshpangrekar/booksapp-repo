<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	    $this->load->model('books_model', 'books_model');
	}

	public function index()
	{
		$data['title'] = 'Dashboard';
		$this->load->view('dashboard', $data);
	}

	public function test()
	{
		$arr=[1, 2, 3, 4, 5];
		$final_array=[];

		for($i=0;$i<count($arr);$i++)
		{
            $res=1;
            for($j=0;$j<count($arr);$j++)
            {
            	if($i!=$j)
            	{
                   $res=$res*$arr[$j];
            	}
            }

            $final_array[]=$res;
		}

		echo "<pre>";
		print_r($final_array);exit();
	}

}
