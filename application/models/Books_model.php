<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books_model extends CI_Model {

  public function getAll()
  {
     $this->db->select('*');
     $this->db->from('books');
     $this->db->order_by('book_id', 'desc');
     $query=$this->db->get();
     $result_array=$query->result_array();
     if($query->num_rows()>0)
     {
        return $result_array;
     }

     else
     {
        return false;
     }
  }


  public function get($book_id)
  {
     $this->db->where('book_id', $book_id);
     $this->db->order_by('book_id', 'desc');
     $query=$this->db->get('books');
     $row=$query->row();
     if($query->num_rows()>0)
     {
        return $row;
     }

     else
     {
        return false;
     }
  }

  public function insert($data)
  {
     $this->db->set($data);
     $this->db->insert('books');
     return true;
  }


  public function update($data, $book_id)
  {
     $this->db->set($data);
     $this->db->where('book_id', $book_id);
     $this->db->update('books');
     return true;
  }

  public function delete($book_id)
  {
     $this->db->where('book_id', $book_id);
     $this->db->delete('books');
     return true;
  }

  public function getAllBooksData()
  {
     $query=$this->db->get('books');
     
     if($query->num_rows() > 0)
     {
        $result_array=$query->result_array();
        return $result_array;
     }

     else
     {
        return null;
     }
  }


}
